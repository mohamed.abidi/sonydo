import {
  PodcastSchema,
  SerieSchema,
  PODCAST_SCHEMA,
} from '../schema/SerieSchema';
const Realm = require('realm');
import _ from 'lodash';

export const addPodcast = async (podcast, cover, piste) => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        realm.write(() => {
          realm.create(PODCAST_SCHEMA, {
            PodcastID: _.get(podcast, 'id', ''),
            PodcastTitle: _.get(podcast, 'title', ''),
            PodcastEtiquetteURI: cover,
            Podcastduration: _.get(podcast, 'duration', ''),
            PodcastSaison: _.get(podcast, 'saison', '').toString(),
            PodcastURI: piste,
            PodcastNumber: _.get(podcast, 'numero', ''),
            SerieID: _.get(podcast, 'serieId', ''),
          });
        });
        resolve(true);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};

export const getAllDatabasePodcast = async () => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        var podcasts = Array.from(realm.objects(PODCAST_SCHEMA));
        resolve(podcasts);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};

export const getDatabasePodcastById = async (Id) => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        console.log(realm);
        // realm = new Realm({ path: 'sonydoDatabase.realm', schema: [PodcastSchema] });
        var podcasts = Array.from(
          realm.objects(PODCAST_SCHEMA).filtered('PodcastID = ' + Id),
        );
        resolve(podcasts);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};

export const deleteDatabasePodcast = async (Id) => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        console.log(realm);
        // realm = new Realm({ path: 'sonydoDatabase.realm', schema: [PodcastSchema] });
        realm.write(() => {
          realm.delete(
            realm.objects(PODCAST_SCHEMA).filtered('PodcastID = ' + Id),
          );
        });

        resolve(true);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};

export const getDatabasePodcastBySerieId = async (Id) => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        // realm = new Realm({ path: 'sonydoDatabase.realm', schema: [PodcastSchema] });
        var podcasts = Array.from(
          realm.objects(PODCAST_SCHEMA).filtered('SerieID = ' + Id),
        );
        resolve(podcasts);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};
