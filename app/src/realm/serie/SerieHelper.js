import {SerieSchema, PodcastSchema, SERIE_SCHEMA} from '../schema/SerieSchema';
const Realm = require('realm');

export const addSerie = async (serie, cover) => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        realm.write(() => {
          realm.create(SERIE_SCHEMA, {
            SerieID: serie.serieId,
            SerieTitle: serie.serieTitle,
            SerieEtiquetteURI: cover,
          });
        });
        resolve(true);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};

export const getAllDatabaseSerie = async () => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        var series = Array.from(realm.objects(SERIE_SCHEMA));
        resolve(series);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};

export const getDatabaseSerieById = async (serie) => {
  try {
    return new Promise(async (resolve, reject) => {
      await Realm.open({schema: [PodcastSchema, SerieSchema]}).then((realm) => {
        var series = Array.from(
          realm.objects(SERIE_SCHEMA).filtered('SerieID = ' + serie),
        );

        if (series.length > 0) resolve(true);
        else resolve(false);
      });
    });
  } catch (ex) {
    reject(ex);
  }
};
