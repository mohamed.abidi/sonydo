export const SERIE_SCHEMA = 'series';
export const PODCAST_SCHEMA = 'podcasts';

export const SerieSchema = {
  name: SERIE_SCHEMA,
  primaryKey: 'SerieID',
  properties: {
    SerieID: 'int',
    SerieTitle: 'string',
    SerieEtiquetteURI: 'string',
  },
};

export const PodcastSchema = {
  name: PODCAST_SCHEMA,
  primaryKey: 'PodcastID',
  properties: {
    PodcastID: 'int',
    PodcastTitle: 'string',
    PodcastEtiquetteURI: 'string',
    Podcastduration: 'string',
    PodcastSaison: 'string',
    PodcastURI: 'string',
    PodcastNumber: 'string',
    SerieID: 'int',
  },
};
