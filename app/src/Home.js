import React, {useEffect} from 'react';

import {Tabs} from './navigation/BottomTabNavigator';
import {connect} from 'react-redux';

const Home = (props) => {
  return <Tabs />;
};

const mapStateToProps = (state) => ({
  playerList: state.playerList,
});

export default connect(mapStateToProps)(Home);
