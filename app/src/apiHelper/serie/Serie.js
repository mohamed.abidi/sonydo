import axios from 'axios';

const url = 'https://api.sonydo.lebackyard.ovh/';

export function getAllSeries() {
  return new Promise(async (resolve, reject) => {
    // const AuthStr = 'Bearer '.concat(userToken);
    axios
      .get(url + 'Series')
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export function getFirstSerie() {
  return new Promise(async (resolve, reject) => {
    // const AuthStr = 'Bearer '.concat(userToken);
    axios
      .get(url + 'Tops')
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
