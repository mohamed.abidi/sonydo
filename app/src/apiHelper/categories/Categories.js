import axios from 'axios';

const url = 'https://api.sonydo.lebackyard.ovh/';

export function getAllCategories() {
  return new Promise(async (resolve, reject) => {
    // const AuthStr = 'Bearer '.concat(userToken);
    axios
      .get(url + 'Categories')
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
