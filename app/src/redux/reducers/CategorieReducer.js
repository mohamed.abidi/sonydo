import {CategorieActionType} from '../constants/ActionTypes';

const initialState = [];

export default function (state = initialState, action) {
  switch (action.type) {
    case CategorieActionType.SET_ALL_CATEGORIES:
      return {
        categories: action.payload,
      };
    default:
      return state;
  }
}
