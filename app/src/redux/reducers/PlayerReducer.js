import {PlayerActionType, SongActionType} from '../constants/ActionTypes';

const initialState = [];

export default function (state = initialState, action) {
  switch (action.type) {
    case PlayerActionType.SET_PLAYER_LIST:
      return {
        playerList: action.payload,
      };

    case SongActionType.SET_CURRENT_SONG:
      return {
        currentSong: action.payload,
      };
    default:
      return state;
  }
}
