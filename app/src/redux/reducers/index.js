import {combineReducers} from 'redux';
import PlayerReducer from './PlayerReducer';
import PlayerModeReducer from './PlayerModeReducer';
import SerieReducer from './SerieReducer';
import CategorieReducer from './CategorieReducer';

export default combineReducers({
  playerList: PlayerReducer,
  serie: SerieReducer,
  mode: PlayerModeReducer,
  categorie: CategorieReducer,
});
