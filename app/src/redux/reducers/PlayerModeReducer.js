import {PlayerModeActionType} from '../constants/ActionTypes';

const initialState = {playerMode: ''};

export default function (state = initialState, action) {
  switch (action.type) {
    case PlayerModeActionType.SET_PLAYER_MODE:
      return {
        playerMode: action.payload,
      };
    default:
      return state;
  }
}
