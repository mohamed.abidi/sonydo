import {SerieActionType, AllSerieActionType} from '../constants/ActionTypes';

const initialState = [];

export default function (state = initialState, action) {
  switch (action.type) {
    case SerieActionType.SET_CURRENT_SERIE:
      return {
        currentSerie: action.payload,
      };
    case AllSerieActionType.SET_ALL_SERIE:
      return {
        series: action.payload,
      };
    default:
      return state;
  }
}
