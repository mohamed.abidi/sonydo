export const PlayerActionType = {
  SET_PLAYER_LIST: 'SET_PLAYER_LIST',
};

export const PlayerModeActionType = {
  SET_PLAYER_MODE: 'SET_PLAYER_MODE',
};

export const SongActionType = {
  SET_CURRENT_SONG: 'SET_CURRENT_SONG',
};

export const SerieActionType = {
  SET_CURRENT_SERIE: 'SET_CURRENT_SERIE',
};

export const AllSerieActionType = {
  SET_ALL_SERIE: 'SET_ALL_SERIE',
};

export const CategorieActionType = {
  SET_ALL_CATEGORIES: 'SET_ALL_CATEGORIES',
};
