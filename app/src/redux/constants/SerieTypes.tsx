// @flow
import {
  SerieActionType,
  AllSerieActionType,
} from './ActionTypes';

/**
 * Action Types
 */

export type SetCurrentSerieActionType = {
  payload: boolean,
  type: typeof SerieActionType.SET_CURRENT_SERIE,
};

export type SetAllSerieActionType = {
  payload: boolean,
  type: typeof AllSerieActionType.SET_ALL_SERIE,
};
