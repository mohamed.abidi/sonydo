// @flow
import {
  PlayerActionType,
  SongActionType,
  SerieActionType,
  PlayerModeActionType,
} from './ActionTypes';

/**
 * Action Types
 */
export type SetPlayerListActionType = {
  payload: boolean,
  type: typeof PlayerActionType.SET_PLAYER_LIST,
};

export type SetPlayerModeActionType = {
  payload: boolean,
  type: typeof PlayerModeActionType.SET_PLAYER_MODE,
};

export type SetCurrentSongActionType = {
  payload: boolean,
  type: typeof SongActionType.SET_CURRENT_SONG,
};

