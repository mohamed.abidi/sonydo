import {CategorieActionType} from './ActionTypes';

export type SetAllCategoriesActionType = {
  payload: boolean,
  type: typeof CategorieActionType.SET_ALL_CATEGORIES,
};
