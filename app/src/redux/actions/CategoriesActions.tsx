import {CategorieActionType} from '../constants/ActionTypes';

import type {SetAllCategoriesActionType} from '../constants/CategorieTypes';

export const seAllCategories = (categories: any): SetAllCategoriesActionType => {
  return {
    type: CategorieActionType.SET_ALL_CATEGORIES,
    payload: categories,
  };
};
