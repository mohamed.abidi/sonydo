import {
  PlayerActionType,
  SongActionType,
  PlayerModeActionType,
} from '../constants/ActionTypes';

import type {
  SetPlayerListActionType,
  SetCurrentSongActionType,
  SetPlayerModeActionType,
} from '../constants/PlayerTypes';

export const setPlayerList = (playerList: any): SetPlayerListActionType => {
  return {
    type: PlayerActionType.SET_PLAYER_LIST,
    payload: playerList,
  };
};

export const setCurrentSong = (track: any): SetCurrentSongActionType => {
  return {
    type: SongActionType.SET_CURRENT_SONG,
    payload: track,
  };
};


export const setPlayerMode = (playerMode: any): SetPlayerModeActionType => {
  return {
    type: PlayerModeActionType.SET_PLAYER_MODE,
    payload: playerMode,
  };
};

