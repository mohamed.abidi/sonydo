import {
  SerieActionType,
  AllSerieActionType,
} from '../constants/ActionTypes';

import type {
  SetAllSerieActionType,
  SetCurrentSerieActionType,
} from '../constants/SerieTypes';




export const setCurrentSerie = (serie: any): SetCurrentSerieActionType => {
  return {
    type: SerieActionType.SET_CURRENT_SERIE,
    payload: serie,
  };
};

export const setAllSerie = (serie: any): SetAllSerieActionType => {
  return {
    type: AllSerieActionType.SET_ALL_SERIE,
    payload: serie,
  };
};


