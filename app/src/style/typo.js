import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export default {
  titleH1: {fontWeight: 'bold', fontSize: wp('7.2%'), fontFamily: 'Montserrat'},
  titleH2: {
    fontWeight: 'bold',
    fontSize: wp('5.2%'),
    fontFamily: 'Montserrat',
  },
  titleH3: {fontWeight: '600', fontSize: wp('4%'), fontFamily: 'Montserrat'},
  titleH4: {fontSize: wp('3.8%'), fontFamily: 'Montserrat', fontWeight: '500'},
  text: {fontSize: wp('3.2%'), fontFamily: 'Montserrat'},
  cta3: {
    fontSize: wp('3.2%'),
    fontWeight: '300',
    fontFamily: 'Montserrat',
  },
  legend1: {fontSize: wp('2.8%'), fontFamily: 'Montserrat'},
  legend2: {fontSize: wp('2.4%'), fontFamily: 'Montserrat'},
};
