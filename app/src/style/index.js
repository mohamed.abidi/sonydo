import colors from './colors';
import typo from './typo';

export {colors, typo};
