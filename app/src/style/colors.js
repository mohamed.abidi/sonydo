export default {
  black: '#000',
  black20: 'rgba(0, 0, 0, 0.2)',
  black40: 'rgba(0, 0, 0, 0.4)',
  black50: 'rgba(0, 0, 0, 0.5)',
  black70: 'rgba(0, 0, 0, 0.7)',
  white: '#fff',
  blackBlur: '#161616',

  // sonydo colors
  primary: '#090E29',
  secondary: '#FF1266',
  navBarre: '#050710',
  tagColor: '#313758',
  secondary_2: '#FF4827',
  fondInput: '#0B1234',
  error: '#FF3B3B',
};
