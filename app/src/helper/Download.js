import RNFetchBlob from 'rn-fetch-blob';

export const downloadFile = (fileUrl) => {
  try {
    return new Promise((resolve, reject) => {
      // Get today's date to add the time suffix in filename
      let date = new Date();
      // File URL which we want to download
      let FILE_URL = fileUrl;
      // Function to get extention of the file url
      let file_ext = getFileExtention(FILE_URL);

      file_ext = '.' + file_ext[0];

      // config: To get response by passing the downloading related options
      // fs: Root directory path to download
      const {config, fs} = RNFetchBlob;
      let RootDir = fs.dirs.DocumentDir;
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          path:
            RootDir +
            '/file_' +
            Math.floor(date.getTime() + date.getSeconds() / 2) +
            file_ext,
          description: 'downloading file...',
          notification: true,
          // useDownloadManager works with Android only
          useDownloadManager: true,
        },
      };
      config(options)
        .fetch('GET', FILE_URL)
        // .uploadProgress((written, total) => {
        //     console.log('uploaded', written / total);
        // }).progress({ count: 10 }, (received, total) => {
        //     console.log('progress', received / total)
        // })
        .then((res) => {
          resolve(res.data);
        });
    });
  } catch (ex) {
    resolve(ex);
  }
};

const getFileExtention = (fileUrl) => {
  // To get the file extension
  return /[.]/.exec(fileUrl) ? /[^.]+$/.exec(fileUrl) : undefined;
};
