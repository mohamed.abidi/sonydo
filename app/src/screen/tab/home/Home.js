import React, {useState, useEffect} from 'react';
import {View, Image, ScrollView, TouchableOpacity} from 'react-native';
import Carrousel from '../../../component/list/Carrousel';
import CategoriesCarrousel from '../../../component/list/CategoriesCarrousel';

import {getAllSeries, getFirstSerie} from '../../../apiHelper/serie/Serie';
import {connect} from 'react-redux';
import {
  setCurrentSerie,
  setAllSerie,
} from '../../../redux/actions/SerieActions';
import styles from './style';
import {scale} from 'react-native-size-matters';
import {colors} from '../../../style';
import {Button, Text} from '@ui-kitten/components';
import {getAllCategories} from '../../../apiHelper/categories/Categories';
import {seAllCategories} from '../../../redux/actions/CategoriesActions';

import {
  MiniPlayerLikeIcon,
  InfoIcon,
} from '../../../component/icon/CustomIcons';

const {pick} = require('lodash');
const API_URL = 'https://api.sonydo.lebackyard.ovh';

// const absoluteURL = `${API_URL}/${relativeUrl}`

const HomeScreen = (props) => {
  const [serie, setSerie] = useState([]);
  const [categories, setCategories] = useState([]);
  const [firstSerie, setFirstSerie] = useState(null);

  useEffect(() => {
    getFirstSerie().then((topSerie) => {
      const top1Serie = pick(topSerie[0], ['film_series']);
      setFirstSerie(top1Serie);
    });
    getAllSeries().then((items) => {
      //props.dispatch(setAllSerie(items));
      setSerie(items);
    });
    getAllCategories().then((items) => {
      setCategories(items);
      props.dispatch(seAllCategories(items));
    });
  }, [props]);

  const onPressItem = (item) => {
    props.dispatch(setCurrentSerie(item));
    props.navigation.navigate('SERIE', {serie: item, title: item.titre});
  };

  const onExpand = () => {
    props.navigation.navigate('CATEGORIES', {categories: categories});
  };

  const onExpandSerie = (series, heading) => {
    props.navigation.navigate('SERIES DETAILS', {
      serie: series,
      title: heading,
    });
  };

  return (
    <ScrollView style={styles.homeContainer}>
      <View style={[styles.card]}>
        {firstSerie && (
          <Image
            style={styles.homeCover}
            source={{
              uri:
                API_URL +
                firstSerie.film_series[0].cover_presentation_375x400.url,
            }}
          />
        )}
        <View style={styles.homeCoverButtonLayout}>
          <TouchableOpacity
            style={styles.homeFavoriteButton}
            activeOpacity={0.7}
            hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}>
            <MiniPlayerLikeIcon size={scale(30)} color={colors.white} />
          </TouchableOpacity>
          <View style={styles.homeListenLayout}>
            <Button style={styles.homeListenButton}>
              {(evaProps) => <Text {...evaProps}>Escuchar</Text>}
            </Button>
          </View>

          <TouchableOpacity
            style={styles.homeFavoriteButton}
            activeOpacity={0.7}
            hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}>
            <InfoIcon size={scale(30)} color={colors.white} />
          </TouchableOpacity>
        </View>
      </View>

      <CategoriesCarrousel
        data={categories}
        heading="Descubre los podcasts"
        onExpand={onExpand}
      />
      <Carrousel
        data={serie}
        heading="Mi lista"
        onPressItem={onPressItem}
        onExpand={onExpandSerie}
      />

      <View style={styles.homeBottomLayout} />
    </ScrollView>
  );
};
const mapStateToProps = (state) => ({
  playerList: state.playerList,
  serie: state.serie,
});

export default connect(mapStateToProps)(HomeScreen);
