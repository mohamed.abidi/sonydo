import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
// eslint-disable-next-line no-unused-vars
import _ from 'lodash';
import ListItem from '../../../component/list/ListItem';
import {connect} from 'react-redux';
import {togglePlayback} from '../../../component/player/PlayerHelper';
import {
  setCurrentSong,
  setPlayerMode,
} from '../../../redux/actions/PlayerActions';
import {getAllDatabasePodcast} from '../../../realm/podcast/PodcastHelper';
import styles from './style';

const Episode = (props) => {
  const [databasePodcast, setDatabasePodcast] = useState([]);

  useEffect(() => {
    getAllDatabasePodcast().then((items) => {
      setDatabasePodcast(
        items.map(function (a) {
          return a.PodcastID;
        }),
      );
    });
  }, []);

  const onPress = (item) => {
    togglePlayback(formatTrackList(props.serie.currentSerie.episodes), item.id);
    props.dispatch(setCurrentSong(item));
    props.dispatch(setPlayerMode('mini'));
  };

  const onItemPress = (item) => {
    togglePlayback(formatTrackList(props.serie.currentSerie.episodes), item.id);
    props.dispatch(setCurrentSong(item));
    props.dispatch(setPlayerMode('full'));
  };

  const onDownload = (item) => {
    getAllDatabasePodcast().then((items) => {
      setDatabasePodcast(
        items.map(function (a) {
          return a.PodcastID;
        }),
      );
    });
  };

  const renderEpisodes = () => {
    const items = [];
    if (props.serie.currentSerie != null) {
      for (let item of formatTrackList(props.serie.currentSerie.episodes)) {
        items.push(
          <ListItem
            onDownload={onDownload}
            key={item.id}
            item={item}
            onPress={onPress}
            onItemPress={onItemPress}
          />,
        );
      }
    }
    return items;
  };

  const formatTrackList = (list) => {
    let trackList = [];
    const API_URL = 'https://api.sonydo.lebackyard.ovh';
    console.log(databasePodcast);
    list.forEach((element) => {
      let downloaded = false;
      if (databasePodcast.includes(element.id)) {
        downloaded = true;
      }
      let track = {
        id: element.id,
        url: API_URL + element.piste_audio.url,
        title: element.titre,
        artist: 'David Chavez',
        description: element.description,
        artwork: API_URL + element.cover_presentation_265x265.url,
        duration: element.duree,
        serieTitle: props.serie.currentSerie.titre,
        numero: element.numero,
        serieId: props.serie.currentSerie.id,
        serieCover:
          API_URL + props.serie.currentSerie.petite_etiquette_75x106.url,
        saison: element.saison,
        downloaded: downloaded,
      };
      trackList.push(track);
    });
    return trackList.sort((a, b) => (a.numero > b.numero ? 1 : -1));
  };

  return <View style={styles.homeContainer}>{renderEpisodes()}</View>;
};

const mapStateToProps = (state) => ({
  playerList: state.playerList,
  serie: state.serie,
});

export default connect(mapStateToProps)(Episode);
