import React from 'react';
import {View} from 'react-native';
import {Text} from '@ui-kitten/components';
// eslint-disable-next-line no-unused-vars
import _ from 'lodash';
import styles from './style';

const Details = (props) => {
  return (
    <View style={styles.homeContainer}>
      <Text style={styles.detailsTextStyle}>Produit par : </Text>
      <Text style={styles.detailsLabelStyle}>
        Lorem ipsum dolor sit amet, consectetur adip
      </Text>
    </View>
  );
};

export default Details;
