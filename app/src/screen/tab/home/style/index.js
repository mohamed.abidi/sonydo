import {StyleSheet} from 'react-native';
import {colors} from '../../../../style';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {verticalScale, scale} from 'react-native-size-matters';

export default StyleSheet.create({
  homeContainer: {
    backgroundColor: colors.primary,
    flex: 1,
  },
  card: {
    elevation: 1,
    borderRadius: 4,
    shadowRadius: 2,
    shadowOpacity: 0.1,
    alignItems: 'center',
    shadowColor: 'black',
    backgroundColor: colors.primary,
    shadowOffset: {width: 0, height: 1},
  },
  homeCover: {
    width: '100%',
    height: verticalScale(280),
    backgroundColor: 'grey',
  },
  homeCoverButtonLayout: {
    marginTop: verticalScale(10),
    flexDirection: 'row',
  },
  textStyle: {
    fontSize: wp('4%'),
    color: colors.white,
    textDecorationLine: 'underline',
    alignSelf: 'center',
    flex: 1,
    textAlign: 'right',
  },
  iconStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: verticalScale(5),
  },
  serieContainer: {
    flex: 1,
    paddingLeft: verticalScale(23),
    paddingRight: verticalScale(23),
    backgroundColor: colors.primary,
  },
  detailsTextStyle: {
    fontSize: wp('4%'),
    fontWeight: '500',
    color: colors.white,
  },
  detailsLabelStyle: {
    marginTop: verticalScale(5),
    fontSize: wp('4%'),
    color: colors.white,
  },
  homeFavoriteButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  homeListenLayout: {
    flex: 2,
    justifyContent: 'center',
  },
  homeListenButton: {
    alignSelf: 'center',
    width: scale(135),
    height: verticalScale(40),
    backgroundColor: colors.secondary,
    borderWidth: 0,
    borderRadius: verticalScale(30),
  },
  homeBottomLayout: {height: verticalScale(120)},
  categoriesDetailsListLayout: {
    flex: 1,
    paddingLeft: verticalScale(10),
    paddingRight: verticalScale(10),
  },
  categoriesDetailsListItem: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: verticalScale(10),
    marginTop: verticalScale(10),
  },
  categoriesDetailsListImageStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: verticalScale(80),
    borderRadius: verticalScale(10),
  },
  categoriesDetailsListTextStyle: {
    color: colors.black,
    textAlign: 'center',
  },
  categoriesDetailsImageStyle: {
    height: verticalScale(80),
    width: '100%',
    borderRadius: verticalScale(10),
  },
  seriesDetailsListImageStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: verticalScale(130),
    borderRadius: verticalScale(10),
  },
  seriesDetailsImageStyle: {
    height: verticalScale(130),
    width: '100%',
    borderRadius: verticalScale(10),
  },
});
