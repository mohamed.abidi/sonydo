import React, {useState} from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import {Text} from '@ui-kitten/components';
// eslint-disable-next-line no-unused-vars
import _ from 'lodash';
import styles from './style';
import {useTheme} from '@react-navigation/native';
import {connect} from 'react-redux';

const CategoriesDetails = (props) => {
  const [categories] = useState(_.get(props, 'categories', []));
  const {typo} = useTheme();

  return (
    <View style={styles.homeContainer}>
      <FlatList
        style={styles.categoriesDetailsListLayout}
        numColumns={2}
        data={categories}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.categoriesDetailsListItem}>
            <View style={styles.categoriesDetailsListImageStyle}>
              <Text
                style={[styles.categoriesDetailsListTextStyle, typo.titleH4]}>
                {item.libelle}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  playerList: state.playerList,
  serie: state.serie,
  mode: state.mode.playerMode,
  categories: state.categorie.categories,
});

export default connect(mapStateToProps)(CategoriesDetails);
