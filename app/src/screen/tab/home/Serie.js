import React, {useState} from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
// eslint-disable-next-line no-unused-vars
import _ from 'lodash';
import {colors} from '../../../style';
import {verticalScale, scale} from 'react-native-size-matters';
import {Tabs} from '../../../navigation/SerieTopNavigation';
import {
  MiniPlayerLikeIcon,
  DownloadIcon,
  ShareIcon,
} from '../../../component/icon/CustomIcons';
import MoreInfo from '../../../component/text/MoreInfo';
import {connect} from 'react-redux';
import styles from './style';

const SerieList = (props) => {
  const API_URL = 'https://api.sonydo.lebackyard.ovh';
  const [serie] = useState(props.route.params.serie);

  return (
    <ScrollView style={styles.serieContainer}>
      <View style={[styles.card]}>
        <Image
          style={styles.homeCover}
          source={{uri: API_URL + serie.cover_presentation_375x400.url}}
        />
      </View>

      <View style={styles.homeCoverButtonLayout}>
        <TouchableOpacity
          activeOpacity={0.7}
          hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
          style={styles.iconStyle}>
          <MiniPlayerLikeIcon size={scale(30)} color={colors.white} />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
          style={styles.iconStyle}>
          <DownloadIcon size={scale(30)} color={colors.white} />
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.7}
          hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
          style={styles.iconStyle}>
          <ShareIcon size={scale(30)} color={colors.white} />
        </TouchableOpacity>

        <Text style={styles.textStyle}>Escuche un extracto</Text>
      </View>

      <MoreInfo
        text={
          'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini veniam, quis nostrud exercitation ullamco labore elit.'
        }
        linesToTruncate={2}
      />
      <Tabs />
      <View style={{height: verticalScale(120)}} />
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  playerList: state.playerList,
  serie: state.serie,
});

export default connect(mapStateToProps)(SerieList);
