import React, {useState} from 'react';
import {View, FlatList, TouchableOpacity, Image} from 'react-native';
// eslint-disable-next-line no-unused-vars
import _ from 'lodash';
import styles from './style';

const SeriesDetails = (props) => {
  const API_URL = 'https://api.sonydo.lebackyard.ovh';
  const [serie] = useState(props.route.params.serie);

  return (
    <View style={styles.homeContainer}>
      <FlatList
        style={styles.categoriesDetailsListLayout}
        numColumns={3}
        data={serie}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.categoriesDetailsListItem}>
            <View style={styles.seriesDetailsListImageStyle}>
              <Image
                style={styles.seriesDetailsImageStyle}
                source={{uri: API_URL + item.moyenne_etiquette_104x150.url}}
              />
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default SeriesDetails;
