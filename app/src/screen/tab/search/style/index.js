import {StyleSheet} from 'react-native';
import {colors} from '../../../../style';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {verticalScale, scale} from 'react-native-size-matters';

export default StyleSheet.create({
  searchContainer: {
    backgroundColor: colors.primary,
    flex: 1,
  },
  searchCategoriesListTitle: {
    color: colors.white,
    marginLeft: verticalScale(20),
    marginTop: verticalScale(10),
  },
});
