import React, {useState} from 'react';
import {View} from 'react-native';
import {Text} from '@ui-kitten/components';
import {useTheme} from '@react-navigation/native';

import CategoriesCarrousel from '../home/CategoriesDetails';
import styles from './style';
import CustomSearchBar from '../../../component/search/SearchBar';

const Search = () => {
  const [searchText, setSearchText] = useState('');
  const {typo} = useTheme();

  const searchFilterFunction = (text) => {
    setSearchText(text);
  };

  return (
    <View style={styles.searchContainer}>
      <CustomSearchBar
        value={searchText}
        onChangeText={(text) => searchFilterFunction(text)}
      />
      <Text style={[styles.searchCategoriesListTitle, typo.titleH3]}>
        Découvrir des podcasts
      </Text>
      <CategoriesCarrousel />
    </View>
  );
};

export default Search;
