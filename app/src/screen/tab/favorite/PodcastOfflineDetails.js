import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {
  deleteDatabasePodcast,
  getDatabasePodcastBySerieId,
} from '../../../realm/podcast/PodcastHelper';
import PodcastItemOffline from '../../../component/list/PodcastItemOffline';
import {verticalScale} from 'react-native-size-matters';
import {setCurrentSong} from '../../../redux/actions/PlayerActions';
import {togglePlayback} from '../../../component/player/PlayerHelper';

const PodcastOfflineDetails = (props) => {
  const [serie, setSerie] = useState(props.route.params.items);

  const renderPodcast = () => {
    const items = [];
    if (serie) {
      for (let item of serie) {
        items.push(
          <PodcastItemOffline
            onDelete={onDelete}
            key={item.id}
            item={item}
            onItemPress={onItemPress}
          />,
        );
      }
    }
    return items;
  };

  const onItemPress = (item) => {
    let trackList = [];

    let track = {
      id: item.PodcastID,
      url: item.PodcastURI,
      title: item.PodcastTitle,
      artist: 'David Chavez',
      description: '',
      artwork: item.PodcastEtiquetteURI,
      duration: item.Podcastduration,
      serieTitle: '',
      numero: '',
      serieId: item.SerieID,
      serieCover: '',
      saison: item.PodcastSaison,
    };
    trackList.push(track);
    togglePlayback(trackList, track.id);
    props.dispatch(setCurrentSong(track));
  };

  const onDelete = (item) => {
    const serieId = item.SerieID;
    deleteDatabasePodcast(item.PodcastID).then((result) => {
      if (result) {
        getDatabasePodcastBySerieId(serieId).then((items) => {
          setSerie(items);
        });
      }
    });
  };

  return <View style={styles.container}>{renderPodcast()}</View>;
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: verticalScale(23),
    paddingRight: verticalScale(23),
    backgroundColor: '#090E29',
    flex: 1,
  },
});

export default connect()(PodcastOfflineDetails);
