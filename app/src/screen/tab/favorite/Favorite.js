import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Carrousel from '../../../component/list/Carrousel';
import CarrouselOffline from '../../../component/list/CarrouselOffline';
import {connect} from 'react-redux';
import {setCurrentSerie} from '../../../redux/actions/SerieActions';
import {getAllDatabaseSerie} from '../../../realm/serie/SerieHelper';
import {verticalScale, scale} from 'react-native-size-matters';

const Favorite = (props) => {
  const [serie, setSerie] = useState([]);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getAllDatabaseSerie().then((items) => {
        console.log(items);
        setSerie(items);
      });
    });

    return unsubscribe;
  }, [props.navigation]);

  const onPressItem = (data, heading) => {
    props.navigation.navigate('FAVORITE FULL SCREEN', {
      items: data,
      title: heading,
    });
  };

  return (
    <View style={styles.container}>
      {serie.length > 0 && (
        <CarrouselOffline
          data={serie}
          heading="Mes Telechargements"
          onPressItem={onPressItem}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: verticalScale(23),
    paddingRight: verticalScale(23),
    backgroundColor: '#090E29',
    flex: 1,
    alignItems: 'center',
  },
});

export default connect()(Favorite);
