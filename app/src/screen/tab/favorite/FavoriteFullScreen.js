import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import ListItemOffline from '../../../component/list/ListItemOffline';
import {verticalScale} from 'react-native-size-matters';

const FavoriteFullScreen = (props) => {
  const [serie] = useState(props.route.params.items);

  const renderEpisodes = () => {
    const items = [];
    if (serie) {
      for (let item of serie) {
        items.push(
          <ListItemOffline
            key={item.SerieID}
            item={item}
            onItemPress={onItemPress}
          />,
        );
      }
    }
    return items;
  };

  const onItemPress = (list, item) => {
    props.navigation.navigate('PODCAST OFFLINE DETAILS', {
      items: list,
      title: item.SerieTitle,
    });
  };

  return <View style={styles.container}>{renderEpisodes()}</View>;
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: verticalScale(23),
    paddingRight: verticalScale(23),
    backgroundColor: '#090E29',
    flex: 1,
  },
});

export default connect()(FavoriteFullScreen);
