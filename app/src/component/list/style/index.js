import {StyleSheet} from 'react-native';
import {colors} from '../../../style';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {verticalScale, scale} from 'react-native-size-matters';
import {withStyles} from '@ui-kitten/components';

export default StyleSheet.create({
  carrouselLayoutStyle: {
    backgroundColor: colors.primary,
    marginTop: verticalScale(10),
    width: '100%',
  },
  carrouselTodoText: {color: colors.white, textDecorationLine: 'underline'},
  carrouselTitleText: {color: colors.white},
  carrouselHeaderLayout: {flexDirection: 'row', paddingLeft: verticalScale(10)},
  carrouselTodoLayout: {position: 'absolute', right: verticalScale(10)},
  carrouselListContentLayout: {
    backgroundColor: colors.primary,
    paddingLeft: verticalScale(10),
    paddingRight: verticalScale(10),
    marginTop: verticalScale(10),
  },
  carrouselCategoriesItemLayout: {
    marginRight: verticalScale(10),
    width: verticalScale(130),
  },
  carrouselCategoriesImageLayout: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: verticalScale(80),
    width: verticalScale(130),
    borderRadius: verticalScale(10),
  },
  carrouselSeriesItemLayout: {
    marginRight: verticalScale(10),
    width: verticalScale(80),
  },
  carrouselSeriesImageLayout: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: verticalScale(120),
    width: verticalScale(80),
    borderRadius: verticalScale(10),
  },
  carrouselSeriesImagestyle: {
    height: verticalScale(120),
    width: verticalScale(80),
    borderRadius: verticalScale(10),
  },
});
