import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import {colors} from '../../style';

import {verticalScale, scale} from 'react-native-size-matters';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {DownloadIcon, DownloadedItem} from '../icon/CustomIcons';
import {
  getDatabasePodcastById,
  addPodcast,
  getAllDatabasePodcast,
} from '../../realm/podcast/PodcastHelper';
import {getDatabaseSerieById, addSerie} from '../../realm/serie/SerieHelper';
import {downloadFile} from '../../helper/Download';

const ListItem = (props) => {
  const [loading, setLoading] = useState(false);

  const downloadPiste = (item) => {
    setLoading(true);
    getDatabasePodcastById(item.id).then((result) => {
      if (result.length > 0) {
        console.log('podcast exist');
        setLoading(false);
      } else {
        downloadFile(item.url).then((result) => {
          downloadFile(item.artwork).then((resultPiste) => {
            addPodcast(item, resultPiste, result).then((result) => {
              props.onDownload(item);
              setLoading(false);
            });
          });
        });
        getDatabaseSerieById(item.serieId).then((serieResult) => {
          if (serieResult) {
            console.log('serie exist');
          } else {
            downloadFile(item.serieCover).then((result) => {
              addSerie(item, result);
            });
          }
        });
      }
    });
  };

  return (
    <TouchableOpacity onPress={() => props.onItemPress(props.item)}>
      <View
        style={{
          flexDirection: 'row',
          marginTop: verticalScale(40),
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => props.onPress(props.item)}
          hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
          <View style={styles.image}>
            <Image source={{uri: props.item.artwork}} style={styles.image} />
          </View>
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            paddingLeft: verticalScale(5),
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: wp('4%'),
              fontWeight: '500',
              color: 'white',
            }}>
            Episodio {props.item.numero}
          </Text>
          <Text
            style={{
              marginTop: verticalScale(5),
              fontSize: wp('4%'),
              color: 'white',
            }}>
            {props.item.title}
          </Text>
          <Text
            style={{
              marginTop: verticalScale(14),
              fontSize: wp('3.5%'),
              color: 'white',
              opacity: 0.7,
            }}>
            {props.item.duration} min
          </Text>
        </View>

        <TouchableOpacity
          style={{justifyContent: 'center'}}
          disabled={props.item.downloaded}
          onPress={() => downloadPiste(props.item)}>
          {props.item.downloaded ? (
            <DownloadedItem size={scale(30)} color={colors.secondary} />
          ) : loading ? (
            <ActivityIndicator color={colors.secondary} size="large" />
          ) : (
            <DownloadIcon size={scale(30)} color={colors.white} />
          )}
        </TouchableOpacity>
      </View>

      <Text
        style={{
          marginTop: verticalScale(5),
          marginLeft: verticalScale(23),
          marginRight: verticalScale(23),
          fontSize: wp('3.5%'),
          color: 'white',
        }}>
        {props.item.description}
      </Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#090E29',
    marginBottom: 16,
    marginTop: 16,
    width: '100%',
  },
  containerContent: {
    backgroundColor: '#090E29',
    paddingLeft: 16,
  },
  heading: {
    fontWeight: '500',
    paddingLeft: 16,
    fontSize: 20,
    color: 'white',
    paddingBottom: 6,
  },
  tagline: {
    color: 'white',
    paddingBottom: 6,
    textAlign: 'center',
  },
  image: {
    height: verticalScale(75),
    width: verticalScale(75),
    borderRadius: verticalScale(10),
  },
  title: {
    color: 'white',
    marginTop: 4,
    textAlign: 'center',
  },
});

export default ListItem;
