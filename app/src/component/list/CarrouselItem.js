import * as React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';

import {images} from '../../mockData/Constants';

const CarrouselItem = ({item}) => {
  <TouchableOpacity
    activeOpacity={0.7}
    hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
    onPress={() => navigation.navigate('PLAYER')}
    style={styles.item}>
    <View style={styles.image}>
      {item.image && <Image source={images[item.image]} style={styles.image} />}
    </View>
  </TouchableOpacity>;
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#090E29',
    marginBottom: 16,
    marginTop: 16,
    width: '100%',
  },
  containerContent: {
    backgroundColor: '#090E29',

    paddingLeft: 16,
  },
  heading: {
    fontWeight: '500',
    paddingLeft: 16,
    fontSize: 20,
    color: 'white',
    paddingBottom: 6,
  },
  tagline: {
    color: 'white',
    paddingBottom: 6,
    textAlign: 'center',
  },
  item: {
    marginRight: 16,
    width: 100,
  },
  image: {
    height: 150,
    width: 100,
  },
  title: {
    color: 'white',
    marginTop: 4,
    textAlign: 'center',
  },
});

export default CarrouselItem;
