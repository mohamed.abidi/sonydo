import * as React from 'react';
import PropTypes from 'prop-types';
import {FlatList, TouchableOpacity, View} from 'react-native';
import styles from './style';
import {useTheme} from '@react-navigation/native';

import {Text} from '@ui-kitten/components';

const CategoriesCarrousel = ({data, heading, onPressItem, onExpand}) => {
  const {typo} = useTheme();
  return (
    <View style={styles.carrouselLayoutStyle}>
      <View style={styles.carrouselHeaderLayout}>
        {heading && (
          <Text style={[styles.carrouselTitleText, typo.titleH3]}>
            {heading}
          </Text>
        )}
        <TouchableOpacity style={styles.carrouselTodoLayout} onPress={onExpand}>
          <Text style={[styles.carrouselTodoText, typo.cta3]}>Todos</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        contentContainerStyle={styles.carrouselListContentLayout}
        data={data}
        horizontal
        keyExtractor={({id}) => id.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.7}
            hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
            style={styles.carrouselCategoriesItemLayout}>
            <View style={styles.carrouselCategoriesImageLayout}>
              <Text style={[typo.titleH4, {textAlign: 'center'}]}>
                {item.libelle}
              </Text>
            </View>
          </TouchableOpacity>
        )}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

CategoriesCarrousel.defaultProps = {
  heading: null,
  tagline: null,
};

CategoriesCarrousel.propTypes = {
  // required
  data: PropTypes.array.isRequired,

  // optional
  heading: PropTypes.string,
  tagline: PropTypes.string,
};

export default CategoriesCarrousel;
