import * as React from 'react';
import PropTypes from 'prop-types';
import {FlatList, Image, TouchableOpacity, View} from 'react-native';

import styles from './style';
import {Text} from '@ui-kitten/components';

import {useTheme} from '@react-navigation/native';

const Carrousel = ({data, heading, tagline, onPressItem}) => {
  const {typo} = useTheme();
  return (
    <View style={styles.carrouselLayoutStyle}>
      <View style={styles.carrouselHeaderLayout}>
        {heading && <Text style={styles.heading}>{heading}</Text>}
        <TouchableOpacity
          style={{
            paddingBottom: 6,
            paddingLeft: 16,
            position: 'absolute',
            right: 16,
          }}
          onPress={() => onPressItem(data, heading)}>
          <Text
            style={{
              fontSize: 16,
              color: 'white',
            }}>
            Todos
          </Text>
        </TouchableOpacity>
      </View>

      {tagline && <Text style={styles.tagline}>{tagline}</Text>}

      <FlatList
        contentContainerStyle={styles.containerContent}
        data={data}
        horizontal
        keyExtractor={(item, index) => item.SerieID.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.7}
            hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
            //onPress={() => onPressItem(item)}
            style={styles.item}>
            <View style={styles.image}>
              {item.SerieEtiquetteURI && (
                <Image
                  source={{uri: item.SerieEtiquetteURI}}
                  style={styles.image}
                />
              )}
            </View>
          </TouchableOpacity>
        )}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

Carrousel.defaultProps = {
  heading: null,
  tagline: null,
};

Carrousel.propTypes = {
  // required
  data: PropTypes.array.isRequired,

  // optional
  heading: PropTypes.string,
  tagline: PropTypes.string,
};

export default Carrousel;
