import * as React from 'react';
import PropTypes from 'prop-types';
import {FlatList, Image, TouchableOpacity, View} from 'react-native';

import styles from './style';
import {Text} from '@ui-kitten/components';

import {useTheme} from '@react-navigation/native';

const API_URL = 'https://api.sonydo.lebackyard.ovh';

const Carrousel = ({data, heading, tagline, onPressItem, onExpand}) => {
  const {typo} = useTheme();
  return (
    <View style={styles.carrouselLayoutStyle}>
      <View style={styles.carrouselHeaderLayout}>
        {heading && (
          <Text style={[styles.carrouselTitleText, typo.titleH3]}>
            {heading}
          </Text>
        )}
        <TouchableOpacity
          style={styles.carrouselTodoLayout}
          onPress={() => onExpand(data, heading)}>
          <Text style={[styles.carrouselTodoText, typo.cta3]}>Todos</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        contentContainerStyle={styles.carrouselListContentLayout}
        data={data}
        horizontal
        keyExtractor={({id}) => id.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.7}
            hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
            onPress={() => onPressItem(item)}
            style={styles.carrouselSeriesItemLayout}>
            <View style={styles.carrouselSeriesImageLayout}>
              {item.moyenne_etiquette_104x150 && (
                <Image
                  source={{uri: API_URL + item.moyenne_etiquette_104x150.url}}
                  style={styles.carrouselSeriesImagestyle}
                />
              )}
            </View>
          </TouchableOpacity>
        )}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

Carrousel.defaultProps = {
  heading: null,
  tagline: null,
};

Carrousel.propTypes = {
  // required
  data: PropTypes.array.isRequired,

  // optional
  heading: PropTypes.string,
  tagline: PropTypes.string,
};

export default Carrousel;
