import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors} from '../../style';

import {verticalScale, scale} from 'react-native-size-matters';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {OfflineItemDelete} from '../icon/CustomIcons';

const API_URL = 'https://api.sonydo.lebackyard.ovh';

const ListItem = (props) => {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: verticalScale(40),
        }}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => props.onItemPress(props.item)}
          hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
          <View style={styles.image}>
            <Image
              source={{uri: props.item.PodcastEtiquetteURI}}
              style={styles.image}
            />
          </View>
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            paddingLeft: verticalScale(5),
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: wp('4%'),
              fontWeight: '500',
              color: 'white',
            }}>
            Episodio {props.item.numero}
          </Text>
          <Text
            style={{
              marginTop: verticalScale(5),
              fontSize: wp('4%'),
              color: 'white',
            }}>
            {props.item.PodcastTitle}
          </Text>
          <Text
            style={{
              marginTop: verticalScale(14),
              fontSize: wp('3.5%'),
              color: 'white',
              opacity: 0.7,
            }}>
            {props.item.Podcastduration} min
          </Text>
        </View>
        <TouchableOpacity
          style={{justifyContent: 'center'}}
          onPress={() => props.onDelete(props.item)}>
          <OfflineItemDelete
            size={scale(30)}
            color={colors.white}></OfflineItemDelete>
        </TouchableOpacity>
      </View>

      <Text
        style={{
          marginTop: verticalScale(5),
          marginLeft: verticalScale(23),
          marginRight: verticalScale(23),
          fontSize: wp('3.5%'),
          color: 'white',
        }}>
        {props.item.description}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#090E29',
    marginBottom: 16,
    marginTop: 16,
    width: '100%',
  },
  containerContent: {
    backgroundColor: '#090E29',
    paddingLeft: 16,
  },
  heading: {
    fontWeight: '500',
    paddingLeft: 16,
    fontSize: 20,
    color: 'white',
    paddingBottom: 6,
  },
  tagline: {
    color: 'white',
    paddingBottom: 6,
    textAlign: 'center',
  },
  image: {
    height: verticalScale(75),
    width: verticalScale(75),
  },
  title: {
    color: 'white',
    marginTop: 4,
    textAlign: 'center',
  },
});

export default ListItem;
