import React, {useState, useEffect} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import {verticalScale} from 'react-native-size-matters';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {getDatabasePodcastBySerieId} from '../../realm/podcast/PodcastHelper';
import {ChevronRight} from '../icon/CustomIcons';

const ListItem = (props) => {
  const [podcast, setPodcast] = useState([]);

  useEffect(() => {
    getDatabasePodcastBySerieId(props.item.SerieID).then((items) => {
      setPodcast(items);
    });
  }, []);
  return (
    <TouchableOpacity
      style={{height: verticalScale(100), marginTop: verticalScale(20)}}
      onPress={() => props.onItemPress(podcast, props.item)}>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <View style={styles.image}>
          <Image
            source={{uri: props.item.SerieEtiquetteURI}}
            style={styles.image}
          />
        </View>
        <View
          style={{
            flex: 1,
            paddingLeft: verticalScale(5),
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: wp('4%'),
              color: 'white',
            }}>
            {props.item.SerieTitle}
          </Text>

          <Text
            style={{
              opacity: 0.7,
              marginTop: verticalScale(10),
              fontSize: wp('3.5%'),
              color: 'white',
            }}>
            {podcast.length} episodes
          </Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <ChevronRight size={30} color="white" />
        </View>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#090E29',
    marginBottom: 16,
    marginTop: 16,
    width: '100%',
  },
  containerContent: {
    backgroundColor: '#090E29',
    paddingLeft: 16,
  },
  heading: {
    fontWeight: '500',
    paddingLeft: 16,
    fontSize: 20,
    color: 'white',
    paddingBottom: 6,
  },
  tagline: {
    color: 'white',
    paddingBottom: 6,
    textAlign: 'center',
  },
  image: {
    height: verticalScale(106),
    width: verticalScale(75),
  },
  title: {
    color: 'white',
    marginTop: 4,
    textAlign: 'center',
  },
});

export default ListItem;
