import React, {useState} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {verticalScale} from 'react-native-size-matters';
import {colors} from '../../style';

const MoreLessComponent = ({truncatedText, fullText}) => {
  const [more, setMore] = useState(false);
  return (
    <Text
      style={{
        marginTop: verticalScale(30),
        fontSize: wp('4%'),
        color: colors.white,
      }}>
      {!more ? `${truncatedText}...` : fullText}
      <TouchableOpacity
        style={{alignContent: 'baseline'}}
        onPress={() => setMore(!more)}>
        <Text
          style={{
            fontSize: wp('4%'),
            color: colors.white,
            alignSelf: 'baseline',
            textDecorationLine: 'underline',
          }}>
          {more ? ' Moins' : ' Plus'}
        </Text>
      </TouchableOpacity>
    </Text>
  );
};

export default MoreLessComponent;
