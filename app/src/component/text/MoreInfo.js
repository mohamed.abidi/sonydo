import React, {useState} from 'react';
import MoreLessComponent from './MoreorLessComponent';
import {Text} from '@ui-kitten/components';
import {verticalScale} from 'react-native-size-matters';

const MoreInfo = ({text, linesToTruncate}) => {
  const [clippedText, setClippedText] = useState(false);
  return clippedText ? (
    <MoreLessComponent truncatedText={clippedText} fullText={text} />
  ) : (
    <Text
      style={{marginTop: verticalScale(30)}}
      //numberOfLines={linesToTruncate}
      ellipsizeMode={'tail'}
      onTextLayout={(event) => {
        //get all lines
        const {lines} = event.nativeEvent;
        //get lines after it truncate
        const currenText = lines
          .splice(0, linesToTruncate)
          .map((line) => line.text)
          .join('');
        //substring with some random digit, this might need more work here based on the font size
        //
        setClippedText(currenText);
      }}>
      {text}
    </Text>
  );
};

export default MoreInfo;
