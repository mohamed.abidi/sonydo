import React from 'react';
import {View} from 'react-native';
import {Input} from '@ui-kitten/components';
import Icon from 'react-native-vector-icons/Ionicons';
import {scale, verticalScale} from 'react-native-size-matters';
import style from './style';

const CustomSearchBar = (props) => {
  return (
    <View style={style.searchBarLayout}>
      <View style={style.searchBarLayoutStyle}>
        <Icon
          style={{marginRight: scale(10), marginLeft: scale(10)}}
          name="search-outline"
          size={verticalScale(20)}
          color={'white'}
        />
        <Input
          style={style.searchInputStyle}
          onChangeText={(text) => props.onChangeText(text)}
          textStyle={style.searchTextStyle}
          value={props.value}
          onBlur={() => console.log('searchBlur')}
          placeholder="Titres, thèmes"
        />
      </View>
    </View>
  );
};
export default CustomSearchBar;
