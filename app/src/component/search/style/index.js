import {StyleSheet} from 'react-native';
import {colors} from '../../../style';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {verticalScale, scale} from 'react-native-size-matters';

export default StyleSheet.create({
  searchBarLayout: {
    height: verticalScale(70),
    backgroundColor: colors.tagColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBarLayoutStyle: {
    flexDirection: 'row',
    marginRight: verticalScale(10),
    marginLeft: verticalScale(10),
    alignItems: 'center',
    height: verticalScale(50),
    borderRadius: verticalScale(50),
    backgroundColor: colors.fondInput,
    paddingLeft: '3%',
  },
  searchInputStyle: {
    flex: 1,
    fontSize: wp('8%'),
    marginRight: verticalScale(15),
    fontStyle: 'italic',
    borderWidth: 0,
    backgroundColor: colors.fondInput,
    fontFamily: 'Montserrat',
  },
  searchTextStyle: {
    fontSize: wp('4.4%'),
    fontStyle: 'italic',
    fontFamily: 'Montserrat',
    color: colors.white,
  },
});
