import React from 'react';
import {createIconSetFromFontello} from 'react-native-vector-icons';
import fontelloConfig from '../../assets/config/config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

export const HomeIcon = (props) => (
  <Icon name="home_actif" size={props.size} color={props.color} />
);

export const FavoriteIcon = (props) => (
  <Icon name="favoris_actif" size={props.size} color={props.color} />
);

export const NewIcon = (props) => (
  <Icon name="bientotdispo_actif" size={props.size} color={props.color} />
);

export const SearchIcon = (props) => (
  <Icon name="search_actif" size={props.size} color={props.color} />
);

export const HomeIconInactif = (props) => (
  <Icon name="home_inactif-1" size={props.size} color={props.color} />
);

export const FavoriteIconInactif = (props) => (
  <Icon name="favoris_inactif" size={props.size} color={props.color} />
);

export const NewIconInactif = (props) => (
  <Icon name="bientotdispo_inactif" size={props.size} color={props.color} />
);

export const SearchIconInactif = (props) => (
  <Icon name="search_inactif" size={props.size} color={props.color} />
);

export const FullPlayerPlayIcon = (props) => (
  <Icon name="bigplayerplay" size={props.size} color={props.color} />
);

export const FullPlayerPauseIcon = (props) => (
  <Icon name="bigplayerpause" size={props.size} color={props.color} />
);

export const DownloadIcon = (props) => (
  <Icon name="download" size={props.size} color={props.color} />
);

export const ShareIcon = (props) => (
  <Icon name="partager" size={props.size} color={props.color} />
);

export const MiniPlayerPauseIcon = (props) => (
  <Icon name="pause" size={props.size} color={props.color} />
);

export const MiniPlayerPlayIcon = (props) => (
  <Icon name="player" size={props.size} color={props.color} />
);

export const MiniPlayerNextIcon = (props) => (
  <Icon name="next" size={props.size} color={props.color} />
);

export const MiniPlayerLikeIcon = (props) => (
  <Icon name="like" size={props.size} color={props.color} />
);

export const BackPlayerIcon = (props) => (
  <Icon name="picto-arrow-leftback" size={props.size} color={props.color} />
);

export const PlayerNextIcon = (props) => (
  <Icon name="bignext" size={props.size} color={props.color} />
);

export const OfflineItemDelete = (props) => (
  <Icon name="bin" size={props.size} color={props.color} />
);

export const DownloadedItem = (props) => (
  <Icon name="download_actif" size={props.size} color={props.color} />
);

export const Logo = (props) => (
  <Icon name="logo" size={props.size} color={props.color} />
);

export const ChevronRight = (props) => (
  <Icon name="angle-right" size={props.size} color={props.color} />
);

export const InfoIcon = (props) => (
  <Icon name="info" size={props.size} color={props.color} />
);
