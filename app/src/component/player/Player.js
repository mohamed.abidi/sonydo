import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import TrackPlayer, {
  useTrackPlayerProgress,
  usePlaybackState,
  useTrackPlayerEvents,
} from 'react-native-track-player';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewPropTypes,
} from 'react-native';

import {Text} from '@ui-kitten/components';

import _ from 'lodash';

import {colors} from '../../style';

import {verticalScale, scale} from 'react-native-size-matters';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {
  PlayerNextIcon,
  BackPlayerIcon,
  FullPlayerPlayIcon,
  FullPlayerPauseIcon,
} from '../icon/CustomIcons';

import SeekBar from '../player/SeekBar';
import {seekTo, getQueue} from './PlayerHelper';

function ProgressBar() {
  const progress = useTrackPlayerProgress();
  console.log('duration = ' + progress.duration);
  console.log('position = ' + progress.position);

  return (
    <SeekBar
      style={{width: '88%', marginTop: verticalScale(30)}}
      min={0}
      max={progress.duration}
      progress={progress.position}
      progressHeight={4}
      progressBackgroundColor={colors.secondary}
      progressColor={colors.white}
      thumbSize={verticalScale(18)}
      thumbColor={colors.secondary}
      onStartTouch={() => {
        console.log('onStartTouch');
      }}
      onProgressChanged={(progress) => seekTo(progress)}
      onStopTouch={() => {
        console.log('onStopTouch');
      }}
    />
  );
}

function ControlButton({title, onPress}) {
  return (
    <TouchableOpacity style={styles.controlButtonContainer} onPress={onPress}>
      <Text style={styles.controlButtonText}>{title}</Text>
    </TouchableOpacity>
  );
}

ControlButton.propTypes = {
  title: PropTypes.string.isRequired,
};

const Player = (props) => {
  const playbackState = usePlaybackState();
  const [serieTitle, setSerieTitle] = useState(
    _.get(props.song, 'serieTitle', ''),
  );
  const [trackTitle, setTrackTitle] = useState(_.get(props.song, 'title', ''));
  const [trackArtwork, setTrackArtwork] = useState(
    _.get(props.song, 'artwork', ''),
  );
  const [pause, setPause] = useState(
    playbackState === TrackPlayer.STATE_PLAYING ||
      playbackState === TrackPlayer.STATE_BUFFERING,
  );

  useEffect(() => {
    setSerieTitle(_.get(props, 'song.serieTitle', ''));
    setTrackTitle(_.get(props, 'song.title', ''));
    setTrackArtwork(_.get(props, 'song.artwork', ''));

    getQueue()
      .then((tracks) => {
        console.log(tracks);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [props]);

  const togglePlayPause = async () => {
    setPause(!pause);
    if (playbackState === TrackPlayer.STATE_PAUSED) {
      await TrackPlayer.play();
    } else {
      await TrackPlayer.pause();
    }
  };

  useTrackPlayerEvents(['playback-state'], async (event) => {
    if (event.state === TrackPlayer.STATE_PAUSED) {
      setPause(true);
    } else if (
      event.state === TrackPlayer.STATE_PLAYING ||
      TrackPlayer.STATE_BUFFERING
    ) {
      setPause(false);
    }
  });

  return (
    <View style={[styles.card]}>
      <TouchableOpacity
        style={{marginTop: verticalScale(60), marginLeft: verticalScale(23)}}
        onPress={props.onPress}>
        <BackPlayerIcon size={verticalScale(20)} color={colors.white} />
      </TouchableOpacity>
      <View style={{alignItems: 'center'}}>
        <Image style={styles.cover} source={{uri: trackArtwork}} />

        <Text style={styles.title}>{serieTitle}</Text>
        <Text style={styles.artist}>{trackTitle}</Text>
        <ProgressBar />

        <View style={styles.controls}>
          <View style={{flex: 1}} />
          <TouchableOpacity
            activeOpacity={0.7}
            hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
            onPress={togglePlayPause}
            style={{flex: 1, alignItems: 'center'}}>
            {pause ? (
              <FullPlayerPlayIcon size={scale(70)} color={colors.secondary} />
            ) : (
              <FullPlayerPauseIcon size={scale(70)} color={colors.secondary} />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
            // onPress={skipToNext}
            style={{flex: 1, justifyContent: 'center'}}>
            <PlayerNextIcon size={scale(25)} color={colors.secondary} />
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          paddingLeft: verticalScale(23),
          paddingRight: verticalScale(23),
        }}>
        <Text style={{color: 'white'}}>
          Lorem ipsum dolor sit amet, consectetur adipisci elit, sed do eiusmod
          tempor incididunt ut labor
        </Text>
      </View>
      <View
        style={{
          marginTop: verticalScale(10),
          paddingLeft: verticalScale(23),
          paddingRight: verticalScale(23),
        }}>
        <Text style={{color: 'white', fontSize: 26}}>Episodes suivants</Text>
      </View>
    </View>
  );
};

Player.propTypes = {
  style: ViewPropTypes.style,
};

Player.defaultProps = {
  style: {},
};

const styles = StyleSheet.create({
  card: {
    elevation: 1,
    borderRadius: 4,
    shadowRadius: 2,
    shadowOpacity: 0.1,
    shadowColor: 'black',
    backgroundColor: colors.primary,
    shadowOffset: {width: 0, height: 1},
  },
  cover: {
    width: verticalScale(265),
    height: verticalScale(265),
    marginTop: 20,
    backgroundColor: 'grey',
  },
  progress: {
    height: 1,
    marginLeft: verticalScale(23),
    marginRight: verticalScale(23),
    marginTop: 10,
    flexDirection: 'row',
  },
  title: {
    alignSelf: 'flex-start',
    marginLeft: verticalScale(23),
    fontSize: wp('5%'),
    color: colors.white,
    marginTop: verticalScale(20),
    fontWeight: '500',
  },
  artist: {
    alignSelf: 'flex-start',
    marginLeft: verticalScale(23),
    fontSize: wp('4%'),
    color: colors.white,
    marginTop: verticalScale(5),
  },
  controls: {
    marginVertical: verticalScale(20),
    flexDirection: 'row',
  },
  controlButtonContainer: {
    flex: 1,
  },
  controlButtonText: {
    color: colors.white,
    fontSize: 18,
    textAlign: 'center',
  },
});

export default Player;
