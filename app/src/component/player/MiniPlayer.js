import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {colors} from '../../style';
import {Text} from '@ui-kitten/components';

import {
  MiniPlayerNextIcon,
  MiniPlayerPauseIcon,
  MiniPlayerLikeIcon,
  MiniPlayerPlayIcon,
} from '../icon/CustomIcons';
import {verticalScale, scale} from 'react-native-size-matters';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {skipToNext} from './PlayerHelper';

import TrackPlayer, {
  usePlaybackState,
  useTrackPlayerEvents,
} from 'react-native-track-player';

import _ from 'lodash';

const MiniPlayer = (props) => {
  const playbackState = usePlaybackState();

  const [serieTitle, setSerieTitle] = useState(
    _.get(props, 'song.serieTitle', ''),
  );
  const [trackTitle, setTrackTitle] = useState(_.get(props, 'song.title', ''));

  const [pause, setPause] = useState(
    playbackState === TrackPlayer.STATE_PLAYING ||
      playbackState === TrackPlayer.STATE_BUFFERING,
  );

  useEffect(() => {
    setSerieTitle(_.get(props, 'song.serieTitle', ''));
    setTrackTitle(_.get(props, 'song.title', ''));
  }, [props]);

  useTrackPlayerEvents(['playback-state'], async (event) => {
    if (event.state === TrackPlayer.STATE_PAUSED) {
      setPause(true);
    } else if (
      event.state === TrackPlayer.STATE_PLAYING ||
      TrackPlayer.STATE_BUFFERING
    ) {
      setPause(false);
    }
  });

  const togglePlayPause = async () => {
    setPause(!pause);
    if (playbackState === TrackPlayer.STATE_PAUSED) {
      await TrackPlayer.play();
    } else {
      await TrackPlayer.pause();
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={props.onPress}
      style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.7}
        hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
        onPress={togglePlayPause}
        style={styles.containerIcon}>
        {pause ? (
          <MiniPlayerPlayIcon size={scale(24)} color={colors.secondary} />
        ) : (
          <MiniPlayerPauseIcon size={scale(24)} color={colors.secondary} />
        )}
      </TouchableOpacity>
      <View style={styles.containerSong}>
        <Text style={styles.title}>
          {/* {`${song.title} · `} */}
          {serieTitle}
        </Text>
        <Text style={styles.artist}>{trackTitle}</Text>
      </View>
      <TouchableOpacity
        activeOpacity={0.7}
        hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
        style={styles.containerIcon}>
        <MiniPlayerLikeIcon size={scale(30)} color={colors.white} />
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={skipToNext}
        hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
        style={styles.containerIcon}>
        <MiniPlayerNextIcon size={scale(30)} color={colors.white} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

MiniPlayer.defaultProps = {
  song: null,
};

MiniPlayer.propTypes = {
  // required

  // optional
  song: PropTypes.shape({
    artist: PropTypes.string,
    title: PropTypes.string,
  }),
};

const styles = StyleSheet.create({
  container: {
    height: verticalScale(50),
    alignSelf: 'center',
    backgroundColor: colors.black,
    borderBottomColor: colors.white,
    borderTopColor: colors.white,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    flexDirection: 'row',
    width: '100%',
  },
  containerIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: verticalScale(5),
  },
  containerSong: {
    flex: 1,
    marginLeft: verticalScale(5),
    overflow: 'hidden',
    justifyContent: 'center',
  },
  title: {
    fontWeight: '500',
    color: colors.white,
    fontSize: wp('4%'),
  },
  artist: {
    color: colors.white,
    fontSize: wp('3.8%'),
  },
});

export default MiniPlayer;
