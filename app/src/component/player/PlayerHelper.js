import TrackPlayer from 'react-native-track-player';

export async function skipToNext() {
  try {
    await TrackPlayer.skipToNext();
  } catch (_) {}
}

export async function skipToPrevious() {
  try {
    await TrackPlayer.skipToPrevious();
  } catch (_) {}
}

export async function initPlayer() {
  await TrackPlayer.setupPlayer({});
  await TrackPlayer.updateOptions({
    stopWithApp: true,
    capabilities: [
      TrackPlayer.CAPABILITY_PLAY,
      TrackPlayer.CAPABILITY_PAUSE,
      TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
      TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
      TrackPlayer.CAPABILITY_STOP,
    ],
    compactCapabilities: [
      TrackPlayer.CAPABILITY_PLAY,
      TrackPlayer.CAPABILITY_PAUSE,
    ],
  });
}

export async function togglePlayback(list, id) {
  await TrackPlayer.reset();
  await TrackPlayer.add(list);
  await TrackPlayer.skip(id.toString());
  await TrackPlayer.play();
}

export async function skipToId(list, id) {
  await TrackPlayer.add(list);
  await TrackPlayer.skip(id.toString());
}

export async function seekTo(seconds) {
  await TrackPlayer.seekTo(seconds);
}

export async function getQueue() {
  return new Promise(async (resolve, reject) => {
    await TrackPlayer.getQueue()
      .then((list) => {
        resolve(list);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
