import React, {useEffect} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
} from 'react-native';

import {connect} from 'react-redux';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import {
  clamp,
  onGestureEvent,
  timing,
  withSpring,
} from 'react-native-redash/lib/module/v1';

import {verticalScale} from 'react-native-size-matters';

import {initPlayer} from '../player/PlayerHelper';

import MiniPlayer from '../player/MiniPlayer';
import Player from '../player/Player';
import {colors} from '../../style';
import {setCurrentSong, setPlayerMode} from '../../redux/actions/PlayerActions';

const {height} = Dimensions.get('window');
const TABBAR_HEIGHT = verticalScale(83);
const MINIMIZED_PLAYER_HEIGHT = verticalScale(50);
const SNAP_TOP = 0;
const SNAP_BOTTOM = height - TABBAR_HEIGHT - MINIMIZED_PLAYER_HEIGHT;
const config = {
  damping: 15,
  mass: 1,
  stiffness: 150,
  overshootClamping: false,
  restSpeedThreshold: 0.1,
  restDisplacementThreshold: 0.1,
};
const {
  Clock,
  Value,
  cond,
  useCode,
  set,
  block,
  not,
  clockRunning,
  interpolateNode,
  Extrapolate,
} = Animated;

const CustomTabBar = (props) => {
  useEffect(() => {
    initPlayer();
  }, []);

  const translationY = new Value(0);
  const velocityY = new Value(0);
  const stateAnimation = new Value(State.UNDETERMINED);
  const offset = new Value(
    props.playerList.currentSong ? SNAP_BOTTOM : height - TABBAR_HEIGHT,
  );
  const goUp = new Value(props.mode === 'full' ? 1 : 0);
  const goDown = new Value(0);
  const gestureHandler = onGestureEvent({
    stateAnimation,
    translationY,
    velocityY,
  });

  const translateY = withSpring({
    value: clamp(translationY, SNAP_TOP, SNAP_BOTTOM),
    velocity: velocityY,
    offset,
    stateAnimation,
    snapPoints: [SNAP_TOP, SNAP_BOTTOM],
    config,
  });
  const translateBottomTab = interpolateNode(translateY, {
    inputRange: [SNAP_TOP, SNAP_BOTTOM],
    outputRange: [TABBAR_HEIGHT, 0],
    extrapolate: Extrapolate.CLAMP,
  });
  const opacity = interpolateNode(translateY, {
    inputRange: [SNAP_BOTTOM - MINIMIZED_PLAYER_HEIGHT, SNAP_BOTTOM],
    outputRange: [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });

  const initAnimation = () => {
    const clock = new Clock();
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useCode(() =>
      block([
        cond(goUp, [
          set(
            offset,
            timing({
              clock,
              from: offset,
              to: SNAP_TOP,
            }),
          ),
          cond(not(clockRunning(clock)), [set(goUp, 0)]),
        ]),
        cond(goDown, [
          set(
            offset,
            timing({
              clock,
              from: offset,
              to: SNAP_BOTTOM,
            }),
          ),
          cond(not(clockRunning(clock)), [set(goDown, 0)]),
        ]),
      ]),
    );
  };

  initAnimation();

  const togglePlayer = () => {
    goUp.setValue(1);
    props.dispatch(setPlayerMode('full'));
  };

  const toggleMiniPlayer = () => {
    goDown.setValue(1);
    props.dispatch(setPlayerMode('mini'));
  };

  const onNext = (track) => {
    props.dispatch(setCurrentSong(track));
  };
  return (
    <>
      <PanGestureHandler {...gestureHandler}>
        <Animated.View
          style={[styles.playerSheet, {transform: [{translateY}]}]}>
          <Player
            song={props.playerList.currentSong}
            onPress={toggleMiniPlayer}
            onNext={onNext}
          />
          {props.playerList.currentSong && (
            <Animated.View
              style={{
                opacity,
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                height: MINIMIZED_PLAYER_HEIGHT,
              }}>
              <MiniPlayer
                song={props.playerList.currentSong}
                onPress={togglePlayer}
                onNext={onNext}
              />
            </Animated.View>
          )}
        </Animated.View>
      </PanGestureHandler>
      <Animated.View
        style={{
          transform: [{translateY: translateBottomTab}],
        }}>
        <SafeAreaView style={styles.container}>
          {props.state.routes.map((route, index) => {
            const {options} = props.descriptors[route.key];

            const focused = props.state.index === index;

            const icon = options.tabBarIcon({focused});

            const onPress = () => {
              const event = props.navigation.emit({
                type: 'tabPress',
                target: route.key,
              });

              if (!focused && !event.defaultPrevented) {
                props.navigation.navigate(route.name);
              }
            };

            return (
              <TouchableOpacity
                key={route.key}
                accessibilityRole="button"
                accessibilityStates={focused ? ['selected'] : []}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                style={styles.tabItemStyle}>
                {icon}
              </TouchableOpacity>
            );
          })}
        </SafeAreaView>
      </Animated.View>
    </>
  );
};

const styles = StyleSheet.create({
  playerSheet: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: colors.primary,
  },
  container: {
    backgroundColor: colors.navBarre,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: TABBAR_HEIGHT,
    flexDirection: 'row',
    borderTopColor: 'black',
    borderWidth: 1,
  },
  tabItemStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state) => ({
  playerList: state.playerList,
  serie: state.serie,
  mode: state.mode.playerMode,
});

export default connect(mapStateToProps)(CustomTabBar);
