import React from 'react';

import Episode from '../screen/tab/home/Episode';
import Details from '../screen/tab/home/Details';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import {verticalScale} from 'react-native-size-matters';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {colors} from '../style';

const Tab = createMaterialTopTabNavigator();

export function Tabs() {
  return (
    <Tab.Navigator
      initialRouteName="Episodios"
      tabBarOptions={{
        showLabel: true,
        labelStyle: {
          fontSize: wp('4.5%'),
          fontWeight: '600',
          color: colors.white,
        },
        style: {
          height: verticalScale(44),
          marginTop: verticalScale(30),
          backgroundColor: colors.primary,
          borderTopWidth: 0,
        },
        activeTintColor: colors.primary,
        inactiveTintColor: colors.white,
        indicatorStyle: {
          backgroundColor: colors.secondary,
          height: 3,
        },
      }}>
      <Tab.Screen name="Episodios" component={Episode} />
      <Tab.Screen name="Detalles" component={Details} />
    </Tab.Navigator>
  );
}
