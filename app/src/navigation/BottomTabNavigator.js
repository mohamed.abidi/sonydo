import React from 'react';

import {
  HomeStackNavigator,
  FavoriteStackNavigator,
  SearchStackNavigator,
} from '../navigation/StackNavigation';
import New from '../screen/tab/new';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {NavigationContainer} from '@react-navigation/native';

import {scale} from 'react-native-size-matters';

import {colors} from '../style';
import {
  HomeIcon,
  FavoriteIcon,
  NewIcon,
  SearchIcon,
  HomeIconInactif,
  FavoriteIconInactif,
  NewIconInactif,
  SearchIconInactif,
} from '../component/icon/CustomIcons';
import * as theme from '../style';

import MyCustomTabBar from '../component/tabbar/MyCustomTabBar';

const Tab = createBottomTabNavigator();

export function Tabs() {
  return (
    <NavigationContainer theme={theme}>
      <Tab.Navigator tabBar={(props) => <MyCustomTabBar {...props} />}>
        <Tab.Screen
          name="Home"
          component={HomeStackNavigator}
          options={{
            tabBarIcon: ({focused}) =>
              focused ? (
                <HomeIcon
                  size={scale(20)}
                  color={colors.secondary}
                  focused={focused}
                />
              ) : (
                <HomeIconInactif
                  size={scale(20)}
                  color={colors.white}
                  focused={focused}
                />
              ),
          }}
        />
        <Tab.Screen
          name="Search"
          component={SearchStackNavigator}
          options={{
            tabBarIcon: ({focused}) =>
              focused ? (
                <SearchIcon
                  size={scale(20)}
                  color={colors.secondary}
                  focused={focused}
                />
              ) : (
                <SearchIconInactif
                  size={scale(20)}
                  color={colors.white}
                  focused={focused}
                />
              ),
          }}
        />
        <Tab.Screen
          name="Favorite"
          component={FavoriteStackNavigator}
          options={{
            tabBarIcon: ({focused}) =>
              focused ? (
                <FavoriteIcon
                  size={scale(20)}
                  color={colors.secondary}
                  focused={focused}
                />
              ) : (
                <FavoriteIconInactif
                  size={scale(20)}
                  color={colors.white}
                  focused={focused}
                />
              ),
          }}
        />
        <Tab.Screen
          name="New"
          component={New}
          options={{
            tabBarIcon: ({focused}) =>
              focused ? (
                <NewIcon
                  size={scale(20)}
                  color={colors.secondary}
                  focused={focused}
                />
              ) : (
                <NewIconInactif
                  size={scale(20)}
                  color={colors.white}
                  focused={focused}
                />
              ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
