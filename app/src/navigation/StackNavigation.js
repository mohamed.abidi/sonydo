import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {verticalScale, scale} from 'react-native-size-matters';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import Home from '../screen/tab/home/Home';
import Serie from '../screen/tab/home/Serie';
import CategoriesDetails from '../screen/tab/home/CategoriesDetails';
import SeriesDetails from '../screen/tab/home/SeriesDetails';

import Favorite from '../screen/tab/favorite/Favorite';
import FavoriteFullScreen from '../screen/tab/favorite/FavoriteFullScreen';
import PdocastOfflineDetails from '../screen/tab/favorite/PodcastOfflineDetails';

import Search from '../screen/tab/search/Search';

import {Logo} from '../component/icon/CustomIcons';

import {Text} from '@ui-kitten/components';

import {colors} from '../style';
import _ from 'lodash';

const Stack = createStackNavigator();

const HEADERHEAIGHT = verticalScale(60);

const screenOptionStyle = (route) => {
  let options = {
    headerStyle: {
      height: verticalScale(HEADERHEAIGHT),
      backgroundColor: colors.navBarre,
      shadowColor: 'transparent',
    },
    headerTitleStyle: {fontSize: wp('3.5%'), color: 'white'},
    headerTintColor: 'white',
    headerBackTitleVisible: false,
    headerTitle: () => <Logo size={scale(30)} color={colors.white} />,
  };
  return options;
};

const serieOptionStyle = (route) => {
  let options = {
    headerStyle: {
      height: verticalScale(HEADERHEAIGHT),
      backgroundColor: colors.navBarre,
      shadowColor: 'transparent',
    },
    headerTitleStyle: {fontSize: wp('3.5%'), color: 'white'},
    headerTintColor: 'white',
    headerTitle: () => (
      <Text style={{color: colors.white}}>
        {_.get(route, 'route.params.title', '')}
      </Text>
    ),
  };
  return options;
};

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="HOME" component={Home} />
      <Stack.Screen name="SERIE" component={Serie} options={serieOptionStyle} />
      <Stack.Screen
        name="CATEGORIES"
        component={CategoriesDetails}
        options={screenOptionStyle}
      />
      <Stack.Screen
        name="SERIES DETAILS"
        component={SeriesDetails}
        options={serieOptionStyle}
      />
    </Stack.Navigator>
  );
};

const FavoriteStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="FAVORITE" component={Favorite} />
      <Stack.Screen
        name="FAVORITE FULL SCREEN"
        component={FavoriteFullScreen}
        options={serieOptionStyle}
      />
      <Stack.Screen
        name="PODCAST OFFLINE DETAILS"
        component={PdocastOfflineDetails}
        options={serieOptionStyle}
      />
    </Stack.Navigator>
  );
};

const SearchStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="SEARCH" component={Search} />
    </Stack.Navigator>
  );
};

export {HomeStackNavigator, FavoriteStackNavigator, SearchStackNavigator};
