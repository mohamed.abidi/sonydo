import React, {useState} from 'react';

import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';

import {Text, Input, Button} from '@ui-kitten/components';

import {verticalScale, scale} from 'react-native-size-matters';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Login = (props) => {
  const [stateEmail, setstateEmail] = useState('');
  const [statePassword, setstatePassword] = useState('');

  return (
    <>
      <View style={{paddingLeft: scale(37), paddingRight: scale(37), flex: 1}}>
        <Text
          style={{
            color: '#433C35',
            fontSize: wp('4.2%'),
            alignSelf: 'center',
            fontWeight: 'bold',
            textAlign: 'center',
            marginTop: verticalScale(50),
          }}>
          Connexion
        </Text>

        <Text
          style={{
            color: '#433C35',
            fontSize: wp('4.2%'),
            fontWeight: '600',
            marginTop: verticalScale(30),
          }}>
          Nom d’utilisateur
        </Text>

        <TextInput
          onChange={(email) => setstateEmail(email)}
          value={stateEmail}
          style={{
            height: verticalScale(40),
            width: '100%',
            borderWidth: 1,
            borderColor: '#A39382',
            alignSelf: 'center',
            marginTop: verticalScale(10),
          }}
        />
        <Text
          style={{
            color: '#433C35',
            fontSize: wp('4.2%'),
            fontWeight: '600',
            marginTop: verticalScale(30),
          }}>
          Mot de passe
        </Text>

        <TextInput
          onChange={(password) => setstatePassword(password)}
          value={statePassword}
          style={{
            height: verticalScale(40),
            width: '100%',
            borderWidth: 1,
            marginTop: verticalScale(10),
            borderColor: '#A39382',
            alignSelf: 'center',
          }}
        />

        <TouchableOpacity
          onPress={() => props.navigation.replace('HomeScreen')}
          style={{
            height: verticalScale(37),
            width: scale(234),
            marginTop: verticalScale(20),
            justifyContent: 'center',
            backgroundColor: '#005EB8',
            alignSelf: 'center',
            borderTopLeftRadius: verticalScale(5),
            borderBottomRightRadius: verticalScale(5),
            borderBottomLeftRadius: verticalScale(5),
          }}>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: wp('4.2%'),
              fontWeight: 'bold',
            }}>
            Connexion
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#090E29',
    marginBottom: 16,
    marginTop: 16,
    width: '100%',
  },
  containerContent: {
    backgroundColor: '#090E29',

    paddingLeft: 16,
  },
  heading: {
    fontWeight: '500',
    paddingLeft: 16,
    fontSize: 20,
    color: 'white',
    paddingBottom: 6,
  },
  tagline: {
    color: 'white',
    paddingBottom: 6,
    textAlign: 'center',
  },
  item: {
    marginRight: 16,
    width: 100,
  },
  image: {
    height: 150,
    width: 100,
  },
  title: {
    color: 'white',
    marginTop: 4,
    textAlign: 'center',
  },
});

export default Login;
