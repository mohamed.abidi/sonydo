import React from 'react';
import Login from './src/Login';
import HomeScreen from './src/Home';
import {Router, Scene} from 'react-native-router-flux';
import * as eva from '@eva-design/eva';
import {ApplicationProvider} from '@ui-kitten/components';
import {Provider} from 'react-redux';
import AppStore from './src/redux/store/Store';

const App = () => {
  return (
    <ApplicationProvider {...eva} theme={eva.light}>
      <Provider store={AppStore}>
        <Router>
          <Scene key="root">
            <Scene
              component={Login}
              hideNavBar={true}
              initial={true}
              key="Login"
              title="Login"
            />
            <Scene
              component={HomeScreen}
              hideNavBar={true}
              key="HomeScreen"
              title="HomeScreen"
            />
          </Scene>
        </Router>
      </Provider>
    </ApplicationProvider>
  );
};

export default App;
